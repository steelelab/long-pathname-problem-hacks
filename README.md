# Long-Pathname-Problem Hacks

## Background

A collection of code for dealing with the inability of Windows with "fully qualified path names" of more than 260 characters:

https://stackoverflow.com/questions/1880321/why-does-the-260-character-path-length-limit-exist-in-windows

Note that this limit is on the FULL path name, including `c:\` and all of the characters in the subfolders. This was a bit problem for us since we use automatically generated folder names based on human-readable timestamps in our measurement software, to which a custom string is added that people use to remember the data settings. This is then repeated again in our data file name (which has the same name as the folder but with a `.dat` at the end.) 

People unfortunately got used to using very long and descriptive custom strings, which can easily result in full paths that exceed the 260 character limit. 

It caused us a lot of pain because python on our measurement computers uses a different library call that allows it to not be limited to 260 characters. But our Synology Drive software, which we use for synchronising our mesurement data to our central file server, uses the windows API that has the MAX_PATH limitation,  it did not copy over our files! (total disaster!). Shockingly, we also failed to copy these over using the copy-paste of windows explorer. I am not even sure if we every recovered the files to our central server! 

## Contents

For now, there is only one small piece of code that looks for all files in subfolders of a specified folder for which the full path is beyond a specifyable MAX_PATH: this will at least allow you to identify the potentially dangerous files and rename them or copy them over in  another way.  

Some thoughts about what could be added:

* Automatically copy files to another (network?) location? 
* Automatically rename them so that they do not exceed MAX_PATH?